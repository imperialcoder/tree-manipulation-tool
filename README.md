# Tree Manipulation Tool
> a complex drag and drop tree manipulation tool

this project allows the ability for anyone to update tree data in realtime and see
a resulting outcome because of that data for downline views

## Before Installing:

make sure yarn is installed:

Mac OSX using brew:
```sh
brew install yarn
```

## Installation


For all Platforms:

```sh
yarn build
```

the output for production should be in the dist folder

## Development setup

```sh
yarn
yarn start
```

you should be able to access the webpage at localhost:8080

### Notes for Developers

This projects file structure is base on [Atomic Design](http://bradfrost.com/blog/post/atomic-web-design/) (mostly just for the `src/components` folder)

Some of the technologies you'll prob want to brush up on before diving in would be:

* [React with Hooks](https://reactjs.org/docs/hooks-intro.html)
* [react-spring](https://www.react-spring.io) - bring your components to life with simple spring animation primitives
* [Redux](https://redux.js.org/) - A predictable state container for JavaScript apps
* [KonvaJS](https://konvajs.org/) - HTML5 2D Canvas Library
* [styled-components](https://www.styled-components.com/) - Visual primitives for the component age
* [RXJS](http://reactivex.io/) - An API for asynchronous programming

for MVP, a tastlist.txt has been provided to show you some of the
features i've been working on or need to be worked on.

I recommend setting up [Prettier](https://prettier.io/docs/en/editors.html) for your code editor for an easier development experience

if you need any help understand certain components or pieces of the code, please email me at jesseg@natr.com

<!-- Markdown link & img dfn's -->
[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[wiki]: https://github.com/yourname/yourproject/wiki
