/* eslint-disable no-case-declarations */
/* eslint-disable no-param-reassign */
import {
  INITIALIZE_TREE_DATA,
  UPDATE_FORMATED_TREE_DATA,
  INITIALIZE_DUMMY_DATA,
  TREE_INDEX_FORWARDS,
  TREE_INDEX_BACKWARDS,
  TREE_SET_LOADING_STATUS
} from '../actionTypes';
import { combineArrayObj } from '../../components/services/tree';

const initialState = {
  rawData: {
    treeData: undefined,
    accountData: undefined
  },
  formatedData: {
    data: undefined,
    history: [],
    index: 0
  },
  nodeSize: 30,
  nodePaddingScale: 3,
  loading: false,
  loaded: false
};

const parseInitData = payload => {
  return combineArrayObj(
    payload.treeData,
    payload.accountData,
    (a, b) => a.accountNumber === b.accountNumber
  );
};

const treeReducer = (state = initialState, action) => {
  switch (action.type) {
    case INITIALIZE_TREE_DATA:
      state = {
        ...state,
        rawData: {
          treeData: null,
          accountData: null
        },
        formatedData: {
          data: null,
          history: [],
          index: 0
        },
        loading: true
      };
      break;
    case INITIALIZE_TREE_DATA.concat('_FULFILLED'):
      state = {
        ...state,
        rawData: {
          ...state.rawData,
          treeData: action.payload.treeData,
          accountData: action.payload.accountData
        },
        formatedData: {
          ...state.formatedData,
          data: parseInitData(action.payload),
          history: [parseInitData(action.payload)]
        },
        loading: false,
        loaded: true
      };
      break;
    case INITIALIZE_DUMMY_DATA:
      state = {
        ...state,
        formatedData: {
          ...state.formatedData,
          data: action.payload,
          history: [action.payload]
        },
        loaded: true
      };
      break;
    case UPDATE_FORMATED_TREE_DATA:
      state = {
        ...state,
        formatedData: {
          ...state.formatedData,
          data: action.payload,
          history: [...state.formatedData.history, action.payload],
          index: state.formatedData.index + 1
        }
      };
      break;
    case TREE_INDEX_FORWARDS:
      const indexForward =
        state.formatedData.index + 1 < state.formatedData.history.length
          ? state.formatedData.index + 1
          : state.formatedData.index;
      state = {
        ...state,
        formatedData: {
          ...state.formatedData,
          data: state.formatedData.history[indexForward],
          index: indexForward
        }
      };
      break;
    case TREE_INDEX_BACKWARDS:
      const indexBackwards =
        state.formatedData.index - 1 >= 0
          ? state.formatedData.index - 1
          : state.formatedData.index;
      state = {
        ...state,
        formatedData: {
          ...state.formatedData,
          data: state.formatedData.history[indexBackwards],
          index: indexBackwards
        }
      };
      break;
    case TREE_SET_LOADING_STATUS:
      state = {
        ...state,
        loading: action.payload
      };
      break;
    default:
      break;
  }
  return state;
};

export default treeReducer;
