/* eslint-disable no-param-reassign */
import {
  STAGE_UPDATE_STAGE_DIMENSIONS,
  STAGE_UPDATE_VIEW_DIMENSIONS
} from '../actionTypes';
import { getWindowDimensions } from '../../components/services/tree';

const initialState = {
  stageDimensions: getWindowDimensions(),
  viewDimensions: {
    width: getWindowDimensions().width * 5,
    height: getWindowDimensions().height * 5
  }
};

const stageReducer = (state = initialState, action) => {
  switch (action.type) {
    case STAGE_UPDATE_STAGE_DIMENSIONS:
      state = {
        ...state,
        stageDimensions: {
          width: action.payload.width || state.viewDimensions.width,
          height: action.payload.height || state.viewDimensions.height
        }
      };
      break;
    case STAGE_UPDATE_VIEW_DIMENSIONS:
      state = {
        ...state,
        viewDimensions: {
          width: action.payload.width || state.viewDimensions.width,
          height: action.payload.height || state.viewDimensions.height
        }
      };
      break;
    default:
      break;
  }
  return state;
};

export default stageReducer;
