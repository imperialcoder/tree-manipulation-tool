/* eslint-disable no-param-reassign */
import { SIDE_MENU_TOGGLE, SIDE_MENU_SET_SELECTED_DATA } from '../actionTypes';

const initialState = {
  toggle: false,
  data: undefined
};

const sideMenuReducer = (state = initialState, action) => {
  switch (action.type) {
    case SIDE_MENU_TOGGLE:
      state = {
        ...state,
        toggle: !state.toggle
      };
      break;
    case SIDE_MENU_SET_SELECTED_DATA:
      state = {
        ...state,
        data: action.payload
      };
      break;
    default:
      break;
  }
  return state;
};

export default sideMenuReducer;
