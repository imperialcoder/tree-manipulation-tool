/* eslint-disable no-unused-vars */
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';

// */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/* //

// ! most of your work will be between these comments ! //

// Application Middleware imports
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

// Application Reducers imports
import treeReducer from './reducers/treeReducer';
import stageReducer from './reducers/stageReducer';
import sideMenuReducer from './reducers/sideMenuReducer';

// Application Epics imports
// ! WIP

// Options for the Store
const initialGlobalState = {};
const reducers = { treeReducer, stageReducer, sideMenuReducer };

// ! uncomment for developemnt
// const middleware = [logger, thunk, promise];
// * or
// ! uncomment for production
const middleware = [thunk, promise];

// */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/* //

// ! DO NOT MODIFY ANYTHING BELLOW UNLESS YOU KNOW WHAT YOU'RE DOING
// the beginning, the end, and the whole universe wrapped in a bow bellow :P
// (its just a default export, nothing too exciting)

// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
  combineReducers(reducers),
  initialGlobalState,
  composeEnhancers(applyMiddleware(...middleware))
);
