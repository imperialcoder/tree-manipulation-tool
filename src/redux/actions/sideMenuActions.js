import { SIDE_MENU_TOGGLE, SIDE_MENU_SET_SELECTED_DATA } from '../actionTypes';

export function toggleSideMenu() {
  return {
    type: SIDE_MENU_TOGGLE
  };
}

export function setSideMenuData(data) {
  return {
    type: SIDE_MENU_SET_SELECTED_DATA,
    payload: data
  };
}
