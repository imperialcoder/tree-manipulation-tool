import {
  STAGE_UPDATE_STAGE_DIMENSIONS,
  STAGE_UPDATE_VIEW_DIMENSIONS
} from '../actionTypes';

export function updateStageDimensions(width, height) {
  return {
    type: STAGE_UPDATE_STAGE_DIMENSIONS,
    payload: {
      width,
      height
    }
  };
}

export function updateViewDimensions(width, height) {
  return {
    STAGE_UPDATE_VIEW_DIMENSIONS,
    payload: {
      width,
      height
    }
  };
}
