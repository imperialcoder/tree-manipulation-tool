/* eslint-disable no-unused-vars */
import {
  INITIALIZE_TREE_DATA,
  INITIALIZE_DUMMY_DATA,
  UPDATE_FORMATED_TREE_DATA,
  TREE_INDEX_FORWARDS,
  TREE_INDEX_BACKWARDS,
  TREE_SET_LOADING_STATUS
} from '../actionTypes';
import dummyData from '../../components/services/dummyData';

import {
  getTreeView,
  getAnalysisBatchId,
  getAccountDetails
} from '../../components/services/tree';

export function initializeTreeData(accountNumber) {
  // todo: possibly change this to an epic?
  return {
    type: INITIALIZE_TREE_DATA,
    // eslint-disable-next-line no-unused-vars
    payload: new Promise((resolve, reject) => {
      try {
        getAnalysisBatchId(accountNumber).subscribe(batchID => {
          getTreeView(batchID).subscribe(treeData => {
            getAccountDetails(batchID).subscribe(accountData => {
              resolve({ treeData, accountData });
            });
          });
        });
      } catch {
        reject();
      }
    })
  };
}

export function initializeDummyData() {
  return {
    type: INITIALIZE_DUMMY_DATA,
    payload: dummyData
  };
}

export function updateFormatedTreeData(formatedData) {
  return {
    type: UPDATE_FORMATED_TREE_DATA,
    payload: formatedData
  };
}

export function treeIndexForwards() {
  return {
    type: TREE_INDEX_FORWARDS
  };
}

export function treeIndexBackwards() {
  return {
    type: TREE_INDEX_BACKWARDS
  };
}

export function setTreeLoadingStatus(boolean) {
  return {
    type: TREE_SET_LOADING_STATUS,
    payload: boolean
  };
}
