const express = require('express');
const os = require('os');

const app = express();
const port = 3000;

app.use(express.static('dist'));

app.get('/api/getUserName', (req, res) =>
  res.send({ useName: os.userInfo().useName })
);

// eslint-disable-next-line no-console
app.listen(port, () => console.log(`Listening on port ${port}`));
