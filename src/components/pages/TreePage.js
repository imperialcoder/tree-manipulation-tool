import React, { useState } from 'react';
import styled from 'styled-components';
import { useSpring, animated } from 'react-spring';
import { Provider } from 'react-redux';
import store from '../../redux/store';
import NavBar from '../organisms/NavBar';
import ScrollableKonvaStage from '../molecules/ScrollableKonvaStage';
import TableMenu from '../molecules/TableMenu';
import SideMenu from '../molecules/SideMenu';
import TreeComp from '../organisms/TreeComp';

document.title = 'Tree';

const Container = styled.div`
  display: flex;
  position: relative;
  flex-direction: column;
  height: 100vh;
  width: 100vw;
  overflow: hidden;
`;

const ContentContainer = styled(animated.div)`
  display: flex;
  flex-direction: column;
  position: relative;
  /* // ! if you modify the navbar height, change the max-height val to compensate ! // */
  max-height: 94vh;
`;

function TreePage() {
  const [initialState, setInitialState] = useState(false);
  const stageRef = React.createRef();

  const contentProps = useSpring({
    opacity: 1,
    height: initialState ? '100%' : '0%',
    from: {
      opacity: 0,
      height: '0%'
    }
  });

  return (
    <Container className="tree-page-container">
      <NavBar initialState={{ get: initialState, set: setInitialState }} />
      <ContentContainer style={contentProps}>
        <ScrollableKonvaStage ref={stageRef}>
          <Provider store={store}>
            <TreeComp stageRef={stageRef} />
          </Provider>
        </ScrollableKonvaStage>
        <TableMenu />
      </ContentContainer>
      <SideMenu />
    </Container>
  );
}

export default TreePage;
