export default [
  {
    GQV: 1,
    accountName: 'billy',
    accountNumber: 0,
    depthLevel: 0,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: -1
  },
  {
    GQV: 1,
    accountName: 'dave',
    accountNumber: 5,
    depthLevel: 1,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 0
  },
  {
    GQV: 1,
    accountName: 'nate',
    accountNumber: 11,
    depthLevel: 2,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 2
  },
  {
    GQV: 1,
    accountName: 'chad',
    accountNumber: 12,
    depthLevel: 2,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 4
  },
  {
    GQV: 1,
    accountName: 'kasey',
    accountNumber: 26,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 14
  },
  {
    GQV: 1,
    accountName: 'nala',
    accountNumber: 30,
    depthLevel: 4,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 26
  },
  {
    GQV: 1,
    accountName: 'stephan',
    accountNumber: 27,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 14
  },
  {
    GQV: 1,
    accountName: 'felix',
    accountNumber: 28,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 14
  },
  {
    GQV: 1,
    accountName: 'oscar',
    accountNumber: 29,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 14
  },
  {
    GQV: 1,
    accountName: 'fredo',
    accountNumber: 13,
    depthLevel: 2,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 4
  },
  {
    GQV: 1,
    accountName: 'chevi',
    accountNumber: 14,
    depthLevel: 2,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 5
  },
  {
    GQV: 1,
    accountName: 'joe',
    accountNumber: 2,
    depthLevel: 1,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 0
  },
  {
    GQV: 1,
    accountName: 'sarah',
    accountNumber: 1,
    depthLevel: 1,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 0
  },
  {
    GQV: 1,
    accountName: 'crystal',
    accountNumber: 8,
    depthLevel: 2,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 2
  },
  {
    GQV: 1,
    accountName: 'mario',
    accountNumber: 6,
    depthLevel: 2,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 1
  },
  {
    GQV: 1,
    accountName: 'ben',
    accountNumber: 3,
    depthLevel: 1,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 0
  },
  {
    GQV: 1,
    accountName: 'pat',
    accountNumber: 10,
    depthLevel: 2,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 3
  },
  {
    GQV: 1,
    accountName: 'karen',
    accountNumber: 16,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 10
  },
  {
    GQV: 1,
    accountName: 'lilo',
    accountNumber: 17,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 10
  },
  {
    GQV: 1,
    accountName: 'stitch',
    accountNumber: 18,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 10
  },
  {
    GQV: 1,
    accountName: 'annie',
    accountNumber: 19,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 10
  },
  {
    GQV: 1,
    accountName: 'olivia',
    accountNumber: 31,
    depthLevel: 4,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 19
  },
  {
    GQV: 1,
    accountName: 'jamal',
    accountNumber: 32,
    depthLevel: 4,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 19
  },
  {
    GQV: 1,
    accountName: 'guy',
    accountNumber: 20,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 10
  },
  {
    GQV: 1,
    accountName: 'karrisa',
    accountNumber: 21,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 10
  },
  {
    GQV: 1,
    accountName: 'sam',
    accountNumber: 22,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 10
  },
  {
    GQV: 1,
    accountName: 'kaydee',
    accountNumber: 23,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 10
  },
  {
    GQV: 1,
    accountName: 'bell',
    accountNumber: 24,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 10
  },
  {
    GQV: 1,
    accountName: 'maricio',
    accountNumber: 25,
    depthLevel: 3,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 10
  },
  {
    GQV: 1,
    accountName: 'robbie',
    accountNumber: 4,
    depthLevel: 1,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 0
  },
  {
    GQV: 1,
    accountName: 'megan',
    accountNumber: 7,
    depthLevel: 2,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 1
  },
  {
    GQV: 1,
    accountName: 'trevor',
    accountNumber: 15,
    depthLevel: 2,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 5
  },
  {
    GQV: 1,
    accountName: 'jeremy',
    accountNumber: 9,
    depthLevel: 2,
    payOut: 1,
    rank: 'grey',
    sponsorAccountNumber: 2
  }
];
