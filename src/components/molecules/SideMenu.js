/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useSpring, animated } from 'react-spring';
import styled from 'styled-components';
import FontAwesome from 'react-fontawesome';
import { toggleSideMenu } from '../../redux/actions/sideMenuActions';
import SideMenuListItem from '../atoms/SideMenuListItem';
import Input from '../atoms/Input';

const Container = styled(animated.div)`
  position: absolute;
  display: flex;
  background-color: #4da6ff;
  box-shadow: 0px 0px 7px #595959;
  margin-top: 5em;
`;

const UnorderedList = styled(animated.ul)`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  list-style: none;
  margin: 0;
  padding: 0.5em;
`;

const MenuOptions = styled.li`
  display: flex;
  flex-direction: column;
  background-color: white;
  border-radius: 25px;
  margin: 0.25em;
  padding: 0.5em;
  user-select: none;
  box-shadow: 0px 0px 7px #595959;
  border: 2px solid #e6e6e6;
`;

const MoveToContainer = styled.div`
  display: flex;
  align-items: center;
`;

const Title = styled.p`
  font-size: 1.2em;
  font-weight: bold;
  margin: 0;
`;

const Icon = styled(FontAwesome)`
  margin: 0.5em;
`;

const MenuButtonToggle = styled(animated.button)`
  cursor: pointer;
  padding: 0.15em;
  outline: 0;
  border: 0;
`;

function SideMenu({ menu, toggleMenu }) {
  const [flag, setFlag] = useState({
    mouseHover: false,
    mouseActive: false
  });

  // useEffect(() => {
  //   console.log(menu.data);
  // }, [menu.data]);

  const ContainerProps = useSpring({
    right: menu.toggle ? '0em' : '-25em',
    from: { right: '-25em' }
  });
  const UnorderedListProps = useSpring({ from: {} });

  const MenuToggleProps = useSpring({
    backgroundColor: flag.mouseHover ? '#1a8cff' : '#4da6ff',
    from: {
      backgroundColor: '#4da6ff'
    }
  });

  const iconProps = useSpring({
    transform: menu.toggle ? 'rotate(180)' : 'rotate(0deg)',
    from: {
      transform: 'rotate(0deg)'
    }
  });

  const handleMouseOver = () => setFlag({ ...flag, mouseHover: true });
  const handleMouseOut = () => setFlag({ ...flag, mouseHover: false });
  const handleMouseDown = () => setFlag({ ...flag, mouseActive: true });
  const handleMouseUp = () => {
    setTimeout(() => setFlag({ ...flag, mouseActive: false }), 150);
  };

  return (
    <Container style={ContainerProps}>
      <MenuButtonToggle
        style={MenuToggleProps}
        onClick={toggleMenu}
        onMouseOver={handleMouseOver}
        onMouseOut={handleMouseOut}
        onFocus={() => {}}
        onBlur={() => {}}
        onMouseDown={handleMouseDown}
        onMouseUp={handleMouseUp}
      >
        <animated.div style={iconProps}>
          <FontAwesome name="angle-double-left" />
        </animated.div>
      </MenuButtonToggle>
      {menu.data && (
        <UnorderedList style={UnorderedListProps}>
          <SideMenuListItem
            iconType="male"
            label="Name:"
            value={menu.data.accountName}
          />
          <SideMenuListItem
            iconType="sort amount up"
            label="Rank:"
            value={menu.data.rank}
          />
          <SideMenuListItem
            iconType="globe"
            label="GQV:"
            value={menu.data.GQV}
          />
          <SideMenuListItem iconType="archive" label="PQV:" value="test" />
          <SideMenuListItem iconType="money" label="TOV:" value="test" />
          <MenuOptions>
            <MoveToContainer>
              <Icon name="arrows-alt" size="lg" />
              <Title>Move To...</Title>
              <Input type="text" name="MoveToInput" />
            </MoveToContainer>
          </MenuOptions>
        </UnorderedList>
      )}
    </Container>
  );
}

SideMenu.propTypes = {
  menu: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleMenu: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  menu: state.sideMenuReducer
});

const mapDispatchToProps = dispatch => ({
  toggleMenu: () => dispatch(toggleSideMenu())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SideMenu);
