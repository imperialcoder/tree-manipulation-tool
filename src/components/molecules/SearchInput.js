/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { useSpring, animated } from 'react-spring';
import FontAwesome from 'react-fontawesome';
import Button from '../atoms/Button';

const Container = styled(animated.div)`
  display: flex;
  flex-direction: column;
  border-width: 2px;
  border-style: solid;
  border-radius: 50px;
  border-color: ${props => props.bordercolor};
  background-color: white;
  margin: 1em;
  padding: 0.25em;
  width: 20em;
  box-shadow: 0px 2px 5px #595959;
`;

const NavContainer = styled(animated.div)`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ChildContainer = styled(animated.div)`
  display: flex;
  align-items: flex-end;
  background-color: blue;
  border-radius: 50px;
  padding: 1em;
`;

const StyledInput = styled(animated.input)`
  max-height: 2em;
  outline-width: 0;
  border: 0;
  border-radius: 50px;
  width: 100%;
  font-size: 1em;
  padding-left: 0.75em;
  padding-right: 0.75em;
  background-color: transparent;
`;
const SearchInput = React.forwardRef(
  (
    {
      name,
      type,
      handleEnter,
      handleClick,
      placeholder,
      primaryColor,
      highlightColor,
      value
    },
    ref
  ) => {
    const [flag, setFlag] = useState({
      focus: false,
      mouseActive: false,
      enterActive: false,
      timer: false
    });

    const [valueState, setValueState] = useState(undefined);

    const ContainerProps = useSpring({
      borderColor: flag.focus ? highlightColor : primaryColor,
      transform: `scale(${flag.mouseActive || flag.enterActive ? 0.99 : 1})`,
      boxShadow: !flag.mouseActive
        ? '0px 0px 7px #595959'
        : '0px 0px 0px #595959',
      from: {
        borderColor: primaryColor,
        transform: `scale(1)`,
        boxShadow: '0px 0px 7px #595959'
      }
    });

    const handleChange = e => {
      const newValue = e.target.value;
    };

    const handleMouseDown = e => setFlag({ ...flag, mouseActive: true });

    const handleMouseUp = e =>
      setTimeout(() => setFlag({ ...flag, mouseActive: false }), 100);

    const handleKeyDown = e => {
      const { keyCode } = e;
      if (keyCode === 13) {
        handleEnter(e);
        setFlag({ ...flag, enterActive: true });
      }
    };

    const handleKeyUp = e => {
      const { keyCode } = e;
      if (keyCode === 13) {
        setFlag({ ...flag, enterActive: false });
      }
    };

    const setFocus = e => {
      const focusType = e.type;
      if (focusType === 'focus') {
        setFlag({ ...flag, focus: true });
      } else if (focusType === 'blur') {
        setFlag({ ...flag, focus: false });
      }
    };

    return (
      <Container
        style={ContainerProps}
        primarycolor={primaryColor}
        bordercolor={ContainerProps.borderColor}
      >
        <NavContainer>
          <StyledInput
            ref={ref}
            name={name}
            type={type}
            placeholder={placeholder}
            value={value}
            onChange={handleChange}
            onFocus={setFocus}
            onBlur={setFocus}
            onKeyDown={handleKeyDown}
            onKeyUp={handleKeyUp}
            onMouseDown={handleMouseDown}
            onMouseUp={handleMouseUp}
          />
          <Button name="search" handleClick={handleClick}>
            <FontAwesome name="search" />
          </Button>
        </NavContainer>
      </Container>
    );
  }
);

SearchInput.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string,
  handleEnter: PropTypes.func,
  handleClick: PropTypes.func,
  placeholder: PropTypes.string,
  primaryColor: PropTypes.string,
  highlightColor: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  value: PropTypes.any
};

SearchInput.defaultProps = {
  name: 'default',
  type: 'text',
  handleEnter: undefined,
  handleClick: undefined,
  placeholder: 'Hello World!',
  primaryColor: '#e6e6e6',
  highlightColor: '#63a70a',
  value: undefined
};

export default SearchInput;
