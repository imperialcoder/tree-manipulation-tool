import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSpring, animated } from 'react-spring';
import { connect } from 'react-redux';
import FontAwesome from 'react-fontawesome';
import {
  treeIndexForwards,
  treeIndexBackwards
} from '../../redux/actions/treeActions';
import Button from '../atoms/Button';
import SelectIndicator from '../atoms/SelectIndicator';

const Container = styled(animated.div)`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: transparent;
  border: 2px solid #e6e6e6;
  border-radius: 50px;
  padding: 0.25em;
  margin: 1em;
  width: ${props => props.width};
  background-color: white;
  box-shadow: 0px 0px 7px #595959;
`;

const ButtonContainer = styled(animated.div)`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

const ButtonChildContainer = styled(animated.div)`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
`;

const ButtonLabel = styled(animated.p)`
  margin: 0;
  padding-left: 0.75em;
  padding-right: 0.75em;
`;

const ButtonSpace = styled.div`
  width: 0.5em;
`;

function TimeMachine({
  style,
  visible,
  width,
  tree,
  indexForwards,
  indexBackwards
}) {
  const props = useSpring({ opacity: visible ? 1 : 0, from: { opacity: 0 } });

  const handleForward = () => indexForwards();

  const handleBackward = () => indexBackwards();

  return (
    <Container
      style={{ ...props, ...style, display: visible ? 'flex' : 'none' }}
      width={width}
    >
      <ButtonContainer>
        <Button name="backward" handleClick={handleBackward}>
          <ButtonChildContainer>
            <FontAwesome name="undo" />
            <ButtonLabel>Undo</ButtonLabel>
          </ButtonChildContainer>
        </Button>
        <ButtonSpace />
        <Button name="forward" handleClick={handleForward}>
          <ButtonChildContainer>
            <ButtonLabel>Redo</ButtonLabel>
            <FontAwesome name="redo" />
          </ButtonChildContainer>
        </Button>
      </ButtonContainer>
      <SelectIndicator
        items={tree.formatedData.history}
        selectedIndex={tree.formatedData.index}
        borderRadius={50}
      />
    </Container>
  );
}

TimeMachine.propTypes = {
  style: PropTypes.objectOf(PropTypes.any),
  visible: PropTypes.bool,
  width: PropTypes.string,
  tree: PropTypes.objectOf(PropTypes.any).isRequired,
  indexForwards: PropTypes.func.isRequired,
  indexBackwards: PropTypes.func.isRequired
};

TimeMachine.defaultProps = {
  style: undefined,
  visible: true,
  width: undefined
};

const mapStateToProps = state => ({
  tree: state.treeReducer
});

const mapDispatchToProps = dispatch => ({
  indexForwards: () => {
    dispatch(treeIndexForwards());
  },
  indexBackwards: () => {
    dispatch(treeIndexBackwards());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TimeMachine);
