import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Stage } from 'react-konva';
import { connect } from 'react-redux';
import { updateStageDimensions } from '../../redux/actions/stageActions';
import Load from '../atoms/Load';

const stagePadding = 50;

// this load container prevents the load symbol to overflow onto other elements
const LoadContainer = styled.div`
  position: relative;
  overflow: auto;
`;

const ScrollContainer = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;
`;

const ViewContainer = styled.div`
  overflow: hidden;
`;

const ScrollableKonvaStage = React.forwardRef(
  ({ stageReducer, treeReducer, setStageDim, children }, ref) => {
    const { viewDimensions, stageDimensions } = stageReducer;
    const { loaded } = treeReducer;

    const scrollContainerRef = React.createRef();

    let resizeTimer = null;

    const handleResize = e => {
      // this updates the stage dimensions after resize
      const { innerWidth, innerHeight } = e.currentTarget;
      if (resizeTimer !== null) {
        clearTimeout(resizeTimer);
      }
      resizeTimer = window.setTimeout(() => {
        setStageDim(innerWidth, innerHeight);
      }, 500);
    };

    const handleScroll = e => {
      // this updates the translation of the canvas to be in view
      // as we dont render the whole view at once
      const scrollContainer = e.currentTarget;
      const stage = ref.current;
      const dx = scrollContainer.scrollLeft - stagePadding;
      const dy = scrollContainer.scrollTop - stagePadding;
      stage.container().style.transform = `translate(${dx}px, ${dy}px)`;
      stage.x(-dx);
      stage.y(-dy);
      stage.children.forEach(layer => layer.batchDraw());
    };

    const scrollToCenter = () => {
      const scrollContainer = scrollContainerRef.current;
      scrollContainer.scrollTo(viewDimensions.width, viewDimensions.height);
      scrollContainer.scrollTo(
        scrollContainer.scrollLeft / 2,
        scrollContainer.scrollTop / 2
      );
    };

    useEffect(() => {
      window.addEventListener('resize', handleResize);
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    });

    useEffect(() => {
      setTimeout(() => scrollToCenter(), 200);
    }, [loaded]);

    return (
      <LoadContainer>
        <Load loaded={loaded} position="absolute" />
        <ScrollContainer ref={scrollContainerRef} onScroll={handleScroll}>
          <ViewContainer
            style={{
              width: viewDimensions.width,
              height: viewDimensions.height
            }}
          >
            <Stage
              ref={ref}
              width={stageDimensions.width + stagePadding * 2}
              height={stageDimensions.height + stagePadding * 2}
            >
              {children}
            </Stage>
          </ViewContainer>
        </ScrollContainer>
      </LoadContainer>
    );
  }
);

ScrollableKonvaStage.propTypes = {
  stageReducer: PropTypes.objectOf(PropTypes.any).isRequired,
  treeReducer: PropTypes.objectOf(PropTypes.any).isRequired,
  setStageDim: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  children: PropTypes.any
};

ScrollableKonvaStage.defaultProps = {
  children: undefined
};

const mapStateToProps = state => ({
  stageReducer: state.stageReducer,
  treeReducer: state.treeReducer
});

const mapDispatchToProps = dispatch => ({
  setStageDim: (width, height) => {
    dispatch(updateStageDimensions(width, height));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { forwardRef: true }
)(ScrollableKonvaStage);
