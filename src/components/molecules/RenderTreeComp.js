/* eslint-disable no-unused-vars */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { uniqueId, cloneDeep } from 'lodash';
import { organizeData, flattenOrganizedData } from '../services/tree';
import LayerComp from '../atoms/LayerComp';
import NodeComp from '../atoms/NodeComp';
import LineComp from '../atoms/LineComp';

function RenderTreeComp({ stageReducer, treeReducer, stageRef, handle }) {
  const { viewDimensions } = stageReducer;
  const { formatedData, nodeSize, nodePaddingScale } = treeReducer;

  const canvasCenter = {
    x: viewDimensions.width / 2,
    y: viewDimensions.height / 2
  };

  // * Core Functions

  const calcNodeWidth = node => {
    let nodeCount = 0;
    const logic = n => {
      const { children } = n;
      // eslint-disable-next-line no-unused-expressions, no-plusplus
      children ? children.forEach(cn => logic(cn)) : nodeCount++;
    };
    logic(node);
    return nodeCount * nodeSize * nodePaddingScale;
  };

  const calcTotalLevelWidth = levelData => {
    let result = 0;
    const reducedLevel = levelData.reduce((p, c) => p.concat(c));
    reducedLevel.forEach(node => {
      const curNodeWidth = calcNodeWidth(node);
      result += curNodeWidth;
    });
    return result;
  };

  const calcTotalTreeWidth = organizedData => {
    const topLevel = organizedData[0][0][0];
    const totalWidth = calcNodeWidth(topLevel);
    return totalWidth;
  };

  // * Offset Calculators

  const calcLevelOffset = (organizedData, levelIndex) => {
    // todo: offset height by how many child nodes there are
    const nodeDim = nodeSize * nodePaddingScale;
    const totalWidth = calcTotalTreeWidth(organizedData);
    return {
      x: canvasCenter.x - totalWidth / 2,
      y: canvasCenter.y + levelIndex * nodeDim - nodeDim
    };
  };

  const calcSponsorOffset = (organizedData, levelIndex, sponsorIndex) => {
    // ! this piece needs to be refined further; where putting a pause on this to hopefully
    // ! complete the rest of the app; concider this a major bug i guess?
    // todo: fix this piece of the app
    // console.log(
    //   `\n\n\n\n*************** level: ${levelIndex} | sponsor: ${sponsorIndex} ***************`
    // );
    const curLevel = organizedData[levelIndex];
    const curSponsor = curLevel[sponsorIndex];
    const totalTreeWidth = calcTotalTreeWidth(organizedData);

    const reduceLevel = levelData => levelData.reduce((p, c) => p.concat(c));

    const prevReducedLevels = organizedData
      .filter((level, index) => index < levelIndex)
      .reverse();

    // console.log('\ncurSponsor:', curSponsor);
    const result = [];
    let selectedParent = curSponsor[0].sponsorAccountNumber;
    prevReducedLevels.forEach((prevLevel, prevLevelIndex) => {
      const curEmptySpace = totalTreeWidth - calcTotalLevelWidth(prevLevel);
      // console.log(
      //   `\n&&&&&&&&&&&&&&&&&&&&& ${prevLevelIndex} &&&&&&&&&&&&&&&&&&&&&&`
      // );
      const prevReducedLevel = reduceLevel(prevLevel);
      const prevEmptySpace = totalTreeWidth - calcTotalLevelWidth(prevLevel);

      // console.log('prevReducedLevel:', prevReducedLevel);
      // console.log('prevEmptySpace:', prevEmptySpace);

      const selectedIndex = prevReducedLevel
        .map(n => n.accountNumber)
        .indexOf(selectedParent);
      // console.log(
      //   'selectedParent:',
      //   prevReducedLevel[selectedIndex].accountName
      // );

      const localResult = prevReducedLevel
        .map(node => calcNodeWidth(node))
        .filter((node, index) => index < selectedIndex)
        .reduce((p, c) => p + c, 0);
      // console.log('localResult:', localResult);
      result.push(localResult);
      if (curEmptySpace > 0) {
        selectedParent = prevReducedLevel[selectedIndex].sponsorAccountNumber;
      }
    });
    // console.log('\n&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');
    const finalResult = result.reduce((p, c) => p + c, 0);
    // console.log('finalResult:', finalResult);
    // console.log('\n************************ end ************************');
    return { x: finalResult, y: 0 };
  };

  const calcNodeOffset = (sponsorData, nodeIndex) => {
    const totalNodeWidth = calcNodeWidth(sponsorData[nodeIndex]);
    const offset = sponsorData
      .filter((node, index) => index < nodeIndex)
      .map(node => calcNodeWidth(node))
      .reduce((prev, cur) => prev + cur, 0);
    const result = totalNodeWidth / 2 + offset;
    return { x: result, y: 0 };
  };

  const calcTotalOffset = (levelOffset, sponsorOffset, nodeOffset) => ({
    x: levelOffset.x + sponsorOffset.x + nodeOffset.x,
    y: levelOffset.y + sponsorOffset.y + nodeOffset.y
  });

  const calcLineOffset = (totalOffset, sponsor) => {
    let result;
    if (sponsor) {
      const sponsorOffset = {
        x: sponsor.canvasPosition.current.x - totalOffset.x,
        y: sponsor.canvasPosition.current.y - totalOffset.y
      };
      result = [0, 0, sponsorOffset.x, sponsorOffset.y];
    }
    return result;
  };

  // * everything that glues the data together

  const dataFormater = data => {
    // if you need to reformat a piece of data
    // before sending it off, here is a good place for that
    const curPosition = data.canvasPosition.current;
    if (curPosition.x + curPosition.y === 0) {
      return {
        ...data,
        canvasPosition: {
          current: data.offset.total,
          previous: data.offset.total
        },
        linePoints: {
          current: data.offset.line,
          previous: data.offset.line
        }
      };
    }
    return {
      ...data,
      canvasPosition: {
        current: data.offset.total,
        previous: data.canvasPosition.current
      },
      linePoints: {
        current: data.offset.line,
        previous: data.linePoints.current
      }
    };
  };

  const createNodeComp = data => {
    // create/return a nodeComp
    Object.assign(data, dataFormater(data));
    return (
      <NodeComp
        key={uniqueId()}
        stageRef={stageRef}
        data={data}
        handle={handle}
        size={nodeSize}
      />
    );
  };

  const createLineComp = data => {
    // create/return a nodeComp
    Object.assign(data, dataFormater(data));
    return (
      <LineComp
        key={uniqueId()}
        points={data.linePoints}
        position={data.canvasPosition}
        accountNumber={data.accountNumber}
      />
    );
  };

  const generateRenderItems = () => {
    // generates the tree to spec
    const result = {
      static: [],
      dynamic: []
    };

    const organizedData = organizeData(formatedData.data);
    const flattendData = flattenOrganizedData(organizedData);
    organizedData.forEach((levelData, levelIndex) => {
      const levelOffset = calcLevelOffset(organizedData, levelIndex);
      levelData.forEach((sponsorData, sponsorIndex) => {
        const sponsorOffset = calcSponsorOffset(
          organizedData,
          levelIndex,
          sponsorIndex
        );
        sponsorData.forEach((nodeData, nodeIndex) => {
          const nodeOffset = calcNodeOffset(sponsorData, nodeIndex);
          const totalOffset = calcTotalOffset(
            levelOffset,
            sponsorOffset,
            nodeOffset
          );
          const sponsor = flattendData.filter(
            node => node.accountNumber === nodeData.sponsorAccountNumber
          )[0];
          const lineOffSet = calcLineOffset(totalOffset, sponsor);
          Object.assign(nodeData, {
            offset: {
              level: levelOffset,
              sponsor: sponsorOffset,
              node: nodeOffset,
              total: totalOffset,
              line: lineOffSet
            }
          });
          // * now to tie everything together
          result.static.push(createNodeComp(nodeData));
          if (sponsor) {
            result.dynamic.push(createLineComp(nodeData));
          }
        });
      });
    });
    return result;
  };

  return (
    <LayerComp stageRef={stageRef} itemsToRender={generateRenderItems()} />
  );
}

RenderTreeComp.propTypes = {
  stageReducer: PropTypes.objectOf(PropTypes.any).isRequired,
  treeReducer: PropTypes.objectOf(PropTypes.any).isRequired,
  stageRef: PropTypes.objectOf(PropTypes.any).isRequired,
  handle: PropTypes.objectOf(PropTypes.func)
};

RenderTreeComp.defaultProps = {
  handle: {}
};

const mapStateToProps = state => ({
  treeReducer: state.treeReducer,
  stageReducer: state.stageReducer
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RenderTreeComp);
