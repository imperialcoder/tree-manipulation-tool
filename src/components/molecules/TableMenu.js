/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSpring, animated } from 'react-spring';
import FontAwesome from 'react-fontawesome';
import { connect } from 'react-redux';
import { flattenOrganizedData } from '../services/tree';
import Button from '../atoms/Button';
import Table from '../atoms/Table';

// the height of the popup table menu in ems
const tableHeight = 25;
const tabHeight = 1.3;

const Container = styled(animated.div)`
  display: flex;
  flex-direction: column;
  width: 100%;
  background-color: #4da6ff;
  box-shadow: 0px 0px 7px #595959;
`;

const ContentHeaderContainer = styled.div`
  display: flex;
`;

const ContentContainer = styled.div`
  display: flex;
  overflow: hidden;
`;

const ContentHeader = styled.p`
  margin: 0;
  padding-bottom: 0.5em;
  width: 100%;
  text-align: center;
  cursor: default;
  user-select: none;
  font-size: 1.5em;
  background-color: ${props => (props.color ? props.color : 'transparent')};
`;

const ScrollContainer = styled.div`
  display: flex;
  width: 100%;
  overflow: auto;
`;

const ButtonBar = styled(animated.button)`
  width: 100%;
  height: ${props => `${props.tabheight}em`};
  border: none;
  outline: none;
  cursor: pointer;
`;

function TableMenu({ tree }) {
  const [tableToggle, setTableToggle] = useState(false);

  const [flag, setFlag] = useState({
    buttonHover: false
  });

  const containerProps = useSpring({
    marginBottom: tableToggle ? '0em' : `-${tableHeight - tabHeight * 0.9}em`,
    from: {
      marginBottom: `-${tableHeight - tabHeight}em`,
      height: `${tableHeight}em`
    }
  });

  const buttonBarProps = useSpring({
    backgroundColor: flag.buttonHover ? '#1a8cff' : '#4da6ff',
    from: {
      backgroundColor: '#4da6ff'
    }
  });

  const iconProps = useSpring({
    transform: tableToggle ? 'rotate(180)' : 'rotate(0deg)',
    from: {
      transform: 'rotate(0deg)'
    }
  });

  // bellow defines how the data will look in the table
  // and what variables from accountData will be assigned
  const createMemberObj = member => ({
    CustID: member.accountNumber,
    Name: member.accountName,
    Rank: member.rank,
    GQV: member.GQV,
    PayOut: member.payOut
  });

  const toggleMenu = () => setTableToggle(!tableToggle);

  const handleMouseEnter = () => setFlag({ ...flag, buttonHover: true });
  const handleMouseLeave = () => setFlag({ ...flag, buttonHover: false });

  const formatToTableData = linearData => {
    const columns = [];
    linearData.forEach(item => {
      const row = [];
      const rowData = createMemberObj(item);
      Object.keys(rowData).forEach(key => row.push(rowData[key]));
      columns.push(row);
    });
    return columns;
  };

  const formatToTableHeader = linearData => {
    const row = [];
    const rowData = createMemberObj(linearData[0]);
    Object.keys(rowData).forEach(key => row.push(key));
    return [row];
  };

  return (
    <Container style={containerProps}>
      <ButtonBar
        type="button"
        style={buttonBarProps}
        onClick={toggleMenu}
        tabheight={tabHeight}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
      >
        <animated.div style={iconProps}>
          <FontAwesome name="angle-double-up" />
        </animated.div>
      </ButtonBar>
      <ContentHeaderContainer>
        <ContentHeader color="aqua">Previous</ContentHeader>
        <ContentHeader color="lightgreen">Current</ContentHeader>
      </ContentHeaderContainer>
      <ContentContainer>
        <ScrollContainer className="table-before-scroll-container">
          {tree.formatedData.data && (
            <Table
              id="table-before"
              headerData={formatToTableHeader(tree.formatedData.data)}
              bodyData={formatToTableData(tree.formatedData.data)}
            />
          )}
        </ScrollContainer>
        <ScrollContainer className="table-after">
          {tree.formatedData.data && (
            <Table
              id="table-after"
              headerData={formatToTableHeader(tree.formatedData.data)}
              bodyData={formatToTableData(tree.formatedData.data)}
            />
          )}
        </ScrollContainer>
      </ContentContainer>
    </Container>
  );
}

TableMenu.propTypes = {
  tree: PropTypes.objectOf(PropTypes.any).isRequired
};

const mapStateToProps = state => ({
  tree: state.treeReducer
});

export default connect(
  mapStateToProps,
  {}
)(TableMenu);
