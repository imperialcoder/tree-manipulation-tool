import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  width: 5em;
  height: 3em;
  background-color: blue;
`;

function Dialog({ text }) {
  return <Container>{text}</Container>;
}

Dialog.propTypes = {
  text: PropTypes.string.isRequired
};

export default Dialog;
