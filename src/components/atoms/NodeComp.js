/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Konva from 'konva';
import { Group, Circle, Rect } from 'react-konva';
import {
  toggleSideMenu,
  setSideMenuData
} from '../../redux/actions/sideMenuActions';
import KonvaNameLabel from './KonvaNameLabel';

// if you need to see the bounding boxes and hit-boxes, enable this
const test = false;

// size scale settings for node elements
const hoverScale = 1.15;
const clickedScale = 0.75;
const hitBoxScale = 1.5;

// color settings for node elements
// (note: only use hex colors, otherwise it will break the animations)
const primaryColor = '#1e90ff';
const secondaryColor = '#008000';

// hovering color settings
// (note: only use hex colors, otherwise it will break the animations)
const primaryHoverColor = '#0080ff';
const secondaryHoverColor = '#009900';

function NodeComp({
  menuData,
  toggleMenu,
  setMenuData,
  stageRef,
  data,
  handle,
  size
}) {
  const stage = stageRef.current;
  const nodeRef = React.createRef();

  // find's itself from the top level component (aka stage)
  const findNode = () => stage.findOne(`.node-${data.accountNumber}`);

  const calcNodeWidth = node => {
    let nodeCount = 0;
    const logic = n => {
      const { children } = n;
      // eslint-disable-next-line no-unused-expressions, no-plusplus
      children ? children.forEach(cn => logic(cn)) : nodeCount++;
    };
    logic(node);
    return nodeCount * size * 3;
  };

  // creates a refrence to the nodes individual objest so you can modify at will
  const getNodeElements = () => {
    const parent = findNode();
    return {
      parent,
      node: parent.findOne('.node-elements'),
      hitBox: parent.findOne('.hit-box'),
      nameLabel: parent.findOne('.name-label')
    };
  };

  const cache = (
    timeout = 0,
    canvasItem = findNode(),
    properties = { offset: 10 }
  ) => {
    setTimeout(() => {
      // eslint-disable-next-line no-console
      test ? console.log('cached:', canvasItem) : null;
      canvasItem.cache(properties);
    }, timeout);
  };

  const deCache = (timeout = 0, canvasItem = findNode()) => {
    setTimeout(() => {
      // eslint-disable-next-line no-console
      test ? console.log('decached:', canvasItem) : null;
      canvasItem.clearCache();
    }, timeout);
  };

  // tweening and caching effects for moving the nodes
  useEffect(() => {
    const node = findNode();
    const { current, previous } = data.canvasPosition;
    const compareX = current.x - previous.x;
    const compareY = current.y - previous.y;
    if (compareX > 0 || (compareX < 0 && compareY > 0) || compareY < 0) {
      const duration = 1.15;
      cache(0, findNode(), {
        offset: 10,
        width: calcNodeWidth(data),
        height: size * 3
      });
      deCache(duration * 1000);
      node.to({
        easing: Konva.Easings.ElasticEaseOut,
        duration,
        x: current.x,
        y: current.y
      });
    }
  }, [nodeRef, data]);

  const handleFunc = (event, type, target = findNode()) => {
    // simple function to tell it's parent component
    // (aka treeComp) how to handle certain events
    Object.keys(handle).forEach(handleKey => {
      const curHandle = handle[handleKey];
      if (handleKey.toLowerCase().includes(type.toLowerCase())) {
        curHandle(event, data, target);
      }
    });
  };

  const moveToLayer = (layerName, target = findNode()) => {
    // this takes the targeted node (or of choice) and moves
    // it to a desired layer of your choosing by the layers name
    const nextLayer = stage.findOne(`.layer-${layerName}`);
    target.moveTo(nextLayer);
    stage.children.forEach(layer => layer.batchDraw());
    switch (layerName) {
      case 'dragging':
        target.findOne('.node-elements').startDrag();
        break;
      default:
        break;
    }
  };

  const handleClick = e => {
    handleFunc(e, 'click');
    const { node } = getNodeElements();
    node.findOne('.outer-circle').to({
      duration: 0.2,
      shadowBlur: 0,
      scaleX: clickedScale,
      scaleY: clickedScale
    });
    node.findOne('.inner-circle').to({
      duration: 0.2,
      scaleX: clickedScale,
      scaleY: clickedScale
    });
    setTimeout(() => {
      node.findOne('.outer-circle').to({
        duration: 0.2,
        shadowBlur: 7,
        scaleX: 1,
        scaleY: 1
      });
      node.findOne('.inner-circle').to({
        duration: 0.2,
        scaleX: 1,
        scaleY: 1
      });
    }, 300);
    setMenuData(data);
    if (!menuData.toggle) {
      toggleMenu();
    }
  };

  const handleMouseDown = e => {
    handleFunc(e, 'mouseDown');
    moveToLayer('dragging');
  };

  const handleMouseOver = e => {
    document.body.style.cursor = 'grab';
    const { node } = getNodeElements();
    node.findOne('.outer-circle').to({
      duration: 0.2,
      fill: primaryHoverColor,
      scaleX: hoverScale,
      scaleY: hoverScale
    });
    node.findOne('.inner-circle').to({
      duration: 0.2,
      fill: secondaryHoverColor,
      scaleX: hoverScale,
      scaleY: hoverScale
    });
    handleFunc(e, 'mouseOver');
  };

  const handleMouseOut = e => {
    document.body.style.cursor = 'default';
    const { node } = getNodeElements();
    node.findOne('.outer-circle').to({
      duration: 0.2,
      fill: primaryColor,
      scaleX: 1,
      scaleY: 1
    });
    node.findOne('.inner-circle').to({
      duration: 0.2,
      fill: secondaryColor,
      scaleX: 1,
      scaleY: 1
    });
    handleFunc(e, 'mouseOut');
  };

  const handleFocus = e => {};

  const handleBlur = e => {};

  const handleDragStart = e => {
    handleFunc(e, 'dragStart');
    document.body.style.cursor = 'grabbing';
  };

  const handleDragMove = e => {
    handleFunc(e, 'dragMove');
    const { node, hitBox, nameLabel } = getNodeElements();
    hitBox.position(node.position());
    nameLabel.position(node.position());
  };

  const handleDragEnd = e => {
    handleFunc(e, 'dragEnd');
    document.body.style.cursor = 'grab';

    Object.keys(getNodeElements()).forEach(elementKey => {
      // this basicly iterates through the nodeElements and selects
      // what styles/animations to give based on the switch statement
      const selectedElement = getNodeElements()[elementKey];
      const duration = 1.15;
      switch (elementKey) {
        case 'parent':
          break;
        default:
          selectedElement.to({
            easing: Konva.Easings.ElasticEaseOut,
            duration,
            x: 0,
            y: 0
          });
          break;
      }
      setTimeout(() => {
        moveToLayer('static');
      }, duration * 1000);
    });
  };

  return (
    <Group
      ref={nodeRef}
      name={`node-${data.accountNumber}`}
      x={data.canvasPosition.previous.x}
      y={data.canvasPosition.previous.y}
    >
      <Rect
        // used to test the childPadding on
        // calculate position (it's a visual)
        name="bound-box"
        x={-(calcNodeWidth(data) - 10) / 2}
        y={-(size * 3 - 10) / 2}
        width={calcNodeWidth(data) - 10}
        height={size * 3 - 10}
        fill="grey"
        stroke="black"
        strokeWidth={5}
        opacity={0.25}
        visible={test}
      />
      <Circle
        name="hit-box"
        radius={size * hitBoxScale}
        opacity={test ? 0.5 : 0}
        fill="red"
      />
      <Group
        name="node-elements"
        onClick={handleClick}
        onMouseDown={handleMouseDown}
        onMouseOver={handleMouseOver}
        onMouseOut={handleMouseOut}
        onFocus={handleFocus}
        onBlur={handleBlur}
        draggable
        onDragStart={handleDragStart}
        onDragMove={handleDragMove}
        onDragEnd={handleDragEnd}
      >
        <Circle
          name="outer-circle"
          radius={size / 2}
          fill={primaryColor}
          shadowColor="black"
          shadowBlur={7}
        />
        <Circle name="inner-circle" radius={size / 4} fill={secondaryColor} />
      </Group>
      <KonvaNameLabel name={data.accountName} size={size} />
    </Group>
  );
}

NodeComp.defaultProps = {
  size: 30
};

NodeComp.propTypes = {
  menuData: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleMenu: PropTypes.func.isRequired,
  setMenuData: PropTypes.func.isRequired,
  stageRef: PropTypes.objectOf(PropTypes.any).isRequired,
  data: PropTypes.objectOf(PropTypes.any).isRequired,
  handle: PropTypes.objectOf(PropTypes.any).isRequired,
  size: PropTypes.number
};

const mapStateToProps = state => ({
  menuData: state.sideMenuReducer
});

const mapDispatchToProps = dispatch => ({
  toggleMenu: () => dispatch(toggleSideMenu()),
  setMenuData: data => dispatch(setSideMenuData(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NodeComp);
