import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { uniqueId } from 'lodash';

const StyledTable = styled.table`
  border-collapse: collapse;
  width: 100%;
`;

const TableHeader = styled.thead``;

const TableBody = styled.tbody``;

const FooterGroup = styled.tfoot``;

const Row = styled.tr`
  background-color: ${props => props.rowColorOdd};
  &:nth-child(even) {
    background-color: ${props => props.rowColorEven};
  }
`;

const Header = styled.th`
  padding: ${props => props.cellPadding};
  background-color: ${props => props.headerColor};
  border-width: ${props => (props.border ? props.borderWidth : '0px')};
  border-color: ${props => props.borderColor};
  border-style: solid;
  font-size: 1em;
`;

const Cell = styled.td`
  padding: ${props => props.cellPadding};
  background-color: transparent;
  border-width: ${props => (props.border ? props.borderWidth : '0px')};
  border-color: ${props => props.borderColor};
  border-style: solid;
  font-size: 1em;
`;

function Table({
  id,
  headerData,
  bodyData,
  footerData,
  headerColor,
  rowColorOdd,
  rowColorEven,
  border,
  borderColor,
  borderWidth,
  cellPadding
}) {
  return (
    <StyledTable id={id}>
      {headerData && (
        <TableHeader>
          {headerData.map(row => (
            <Row
              key={uniqueId()}
              rowColorEven={rowColorEven}
              rowColorOdd={rowColorOdd}
            >
              {row.map(data => (
                <Header
                  key={uniqueId()}
                  border={border}
                  borderColor={borderColor}
                  borderWidth={borderWidth}
                  cellPadding={cellPadding}
                  headerColor={headerColor}
                >
                  {data}
                </Header>
              ))}
            </Row>
          ))}
        </TableHeader>
      )}
      {bodyData && (
        <TableBody>
          {bodyData.map(row => (
            <Row
              key={uniqueId()}
              rowColorEven={rowColorEven}
              rowColorOdd={rowColorOdd}
            >
              {row.map(data => (
                <Cell
                  key={uniqueId()}
                  border={border}
                  borderColor={borderColor}
                  borderWidth={borderWidth}
                  cellPadding={cellPadding}
                >
                  {data}
                </Cell>
              ))}
            </Row>
          ))}
        </TableBody>
      )}
      {footerData && (
        <FooterGroup>
          {footerData.map(row => (
            <Row
              key={uniqueId()}
              rowColorEven={rowColorEven}
              rowColorOdd={rowColorOdd}
            >
              {row.map(data => (
                <Cell
                  key={uniqueId()}
                  border={border}
                  borderColor={borderColor}
                  borderWidth={borderWidth}
                  cellPadding={cellPadding}
                >
                  {data}
                </Cell>
              ))}
            </Row>
          ))}
        </FooterGroup>
      )}
    </StyledTable>
  );
}

Table.propTypes = {
  id: PropTypes.string.isRequired,
  headerData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.any)),
  bodyData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.any)).isRequired,
  footerData: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.any)),
  headerColor: PropTypes.string,
  rowColorOdd: PropTypes.string,
  rowColorEven: PropTypes.string,
  borderColor: PropTypes.string,
  borderWidth: PropTypes.string,
  cellPadding: PropTypes.string,
  border: PropTypes.bool
};

Table.defaultProps = {
  headerData: undefined,
  footerData: undefined,
  headerColor: 'grey',
  rowColorOdd: 'white',
  rowColorEven: '#dddddd',
  borderColor: 'black',
  borderWidth: '2px',
  cellPadding: '0.5em',
  border: true
};

export default Table;
