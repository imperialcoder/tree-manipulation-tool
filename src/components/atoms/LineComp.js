/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { PropTypes } from 'prop-types';
import Konva from 'konva';
import { Line } from 'react-konva';

function LineComp({
  points,
  position,
  accountNumber,
  type,
  lineCurveOffset,
  stroke,
  tension,
  width
}) {
  const lineRef = React.createRef();

  useEffect(() => {
    const line = lineRef.current;
    const { current, previous } = position;
    const compareX = current.x - previous.x;
    const compareY = current.y - previous.y;
    if (compareX > 0 || (compareX < 0 && compareY > 0) || compareY < 0) {
      const duration = 1.15;
      line.to({
        easing: Konva.Easings.ElasticEaseOut,
        duration,
        x: current.x,
        y: current.y
      });
    }
  }, [lineRef, position]);

  const calcType = p => {
    const calcMid = (startN, endN, offset = 0) => (startN + endN + offset) / 2;
    if (type.includes('curved')) {
      const start = p.slice(0, 2);
      const end = p.slice(2, 4);
      const middle = [
        calcMid(start[0], end[0]) + calcMid(start[0], end[0]) / 2,
        calcMid(start[1], end[1]) - calcMid(start[1], end[1]) / 2
      ];
      const result = start.concat(middle, end);
      // console.log(start, middle, end);
      return result;
    }
    return p;
  };

  return (
    <Line
      ref={lineRef}
      name={'line-'.concat(String(accountNumber))}
      stroke={stroke}
      strokeWidth={width}
      tension={tension}
      lineCap="round"
      lineJoin="round"
      x={position.previous.x}
      y={position.previous.y}
      points={calcType(points.previous)}
    />
  );
}

LineComp.defaultProps = {
  type: 'straight',
  lineCurveOffset: 50,
  stroke: 'black',
  tension: 0.5,
  width: 2
};

LineComp.propTypes = {
  points: PropTypes.objectOf(
    PropTypes.arrayOf(PropTypes.number),
    PropTypes.arrayOf(PropTypes.number)
  ).isRequired,
  position: PropTypes.objectOf(
    PropTypes.objectOf(PropTypes.number, PropTypes.number),
    PropTypes.objectOf(PropTypes.number, PropTypes.number)
  ).isRequired,
  accountNumber: PropTypes.number.isRequired,
  type: PropTypes.string,
  lineCurveOffset: PropTypes.number,
  stroke: PropTypes.string,
  tension: PropTypes.number,
  width: PropTypes.number
};

export default LineComp;
