import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSpring, animated } from 'react-spring';

const StyledInput = styled(animated.input)`
  box-shadow: 0px 0px 7px #595959;
  border: 2px solid #e6e6e6;
  border-radius: 50px;
  outline: 0;
  min-height: 2em;
  min-width: 2em;
  font-size: 1em;
  padding-left: 0.75em;
  padding-right: 0.75em;
`;

function Input({ type, name, value }) {
  const [flag, setFlag] = useState({
    mouseHover: false,
    mouseActive: false
  });

  const StyledInputProps = useSpring({ from: {} });

  const handleMouseDown = () => setFlag({ ...flag, mouseActive: true });
  const handleMouseUp = () => {
    setTimeout(() => {
      setFlag({ ...flag, mouseActive: false });
    }, 100);
  };

  const handleMouseOver = () => setFlag({ ...flag, mouseHover: true });
  const handleMouseOut = () => setFlag({ ...flag, mouseHover: false });

  const handleFocus = () => {};
  const handleBlur = () => {};

  const handleKeyDown = e => {
    const { keyCode } = e;
    // eslint-disable-next-line no-console
    console.log(`keyDown: ${keyCode}`);
  };
  const handleKeyUp = e => {
    const { keyCode } = e;
    // eslint-disable-next-line no-console
    console.log(`keyUp: ${keyCode}`);
  };

  return (
    <StyledInput
      style={StyledInputProps}
      type={type}
      name={name}
      value={value}
      onMouseDown={handleMouseDown}
      onMouseUp={handleMouseUp}
      onMouseOver={handleMouseOver}
      onMouseOut={handleMouseOut}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onKeyDown={handleKeyDown}
      onKeyUp={handleKeyUp}
    />
  );
}

Input.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  value: PropTypes.any
};

Input.defaultProps = {
  value: undefined
};

export default Input;
