/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSprings, animated } from 'react-spring';
import { uniqueId } from 'lodash';

// this is mostly used for the time machine

const Container = styled(animated.div)`
  display: flex;
  justify-content: space-evenly;
  width: calc(${props => props.width});
  margin-top: 6px;
  margin-bottom: -6px;
`;
const Indicator = styled(animated.div)`
  height: 3px;
  width: 100%;
  background-color: #63a70a;
  border-radius: 50px;
  margin-left: 2px;
  margin-right: 2px;
`;

function SelectIndicator({ items, selectedIndex, borderRadius }) {
  const [springs, set, stop] = useSprings(items.length, index => ({
    opacity: selectedIndex === index ? 1 : 0
  }));

  useEffect(() => {
    set(index => ({ opacity: selectedIndex === index ? 1 : 0 }));
    return () => {
      setTimeout(() => stop(), 500);
    };
  }, [selectedIndex, set, stop]);

  return (
    <Container width={`100% - ${borderRadius / 2}px`}>
      {springs.map(props => (
        <Indicator style={props} key={uniqueId()} />
      ))}
    </Container>
  );
}

SelectIndicator.propTypes = {
  items: PropTypes.arrayOf(PropTypes.any).isRequired,
  selectedIndex: PropTypes.number.isRequired,
  borderRadius: PropTypes.number.isRequired
};

export default SelectIndicator;
