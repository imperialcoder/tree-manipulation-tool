/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Text, Label } from 'react-konva';
import { startCase } from 'lodash';

function KonvaTextBox({ name, size }) {
  const [textHeight, setTextHeight] = useState(0);
  const textRef = React.createRef();

  const formatName = rawNameString => rawNameString;

  useEffect(() => {
    const text = textRef.current;
    if (text.height() !== textHeight) {
      setTextHeight(text.height());
    }
  }, [textRef]);

  // todo: in next meeting, ask about how they want names to be parsed
  // * esspecially for long names
  // possible algrithum:
  // split by comma
  // then split those segments by space

  return (
    <Label name="name-label">
      <Text
        ref={textRef}
        x={-((size * 3) / 2)}
        y={-(20 + textHeight)}
        text={formatName(name)}
        fontSize={size / 2.2}
        align="center"
        verticalAlign="bottom"
        width={size * 3}
      />
    </Label>
  );
}

KonvaTextBox.propTypes = {
  name: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired
};

export default KonvaTextBox;
