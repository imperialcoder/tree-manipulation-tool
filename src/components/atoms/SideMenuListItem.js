/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSpring, animated } from 'react-spring';
import styled from 'styled-components';
import FontAwesome from 'react-fontawesome';

const ListItem = styled(animated.li)`
  display: flex;
  position: relative;
  justify-content: center;
  align-items: center;
  background-color: lightgreen;
  border-radius: 50px;
  margin: 0.25em;
  width: 20em;
  height: 2em;
  user-select: none;
  box-shadow: 0px 0px 7px #595959;
  border: 2px solid #e6e6e6;
`;

const CopyOverlay = styled(animated.div)`
  display: flex;
  position: absolute;
  align-items: center;
  background-color: white;
  border-radius: 50px;
  width: 100%;
  height: 100%;
  user-select: none;
  cursor: pointer;
`;

const LabelIcon = styled(FontAwesome)`
  width: 2em;
  text-align: center;
`;

const LabelTitle = styled.p`
  font-size: 1.2em;
  width: 4em;
  font-weight: bold;
`;

const LabelInput = styled.input`
  background-color: transparent;
  outline: 0;
  border: 0;
  text-align: center;
  cursor: ${props => (props.readOnly ? 'pointer' : '')};
`;

const CopiedText = styled.p`
  margin: 0;
  font-weight: bold;
`;

function SideMenuListItem({ value, iconType, label, readOnly }) {
  const [flag, setFlag] = useState({
    mouseHover: false,
    mouseActive: false,
    mouseActiveFade: false
  });

  const ListItemProps = useSpring({
    transform: flag.mouseActive ? 'scale(0.9)' : 'scale(1)',
    borderColor: flag.mouseHover ? '#63a70a' : '#e6e6e6',
    from: { transform: 'scale(1)', borderColor: '#e6e6e6' }
  });
  const LabelProps = useSpring({
    opacity: flag.mouseActiveFade ? 0 : 1,
    from: { opacity: 1 }
  });

  const handleMouseOver = () => setFlag({ ...flag, mouseHover: true });
  const handleMouseOut = () => setFlag({ ...flag, mouseHover: false });

  const handleFocus = () => {};
  const handleBlur = () => {};

  const handleMouseDown = () =>
    setFlag({ ...flag, mouseActive: true, mouseActiveFade: true });
  const handleMouseUp = () => {
    setTimeout(() => setFlag({ ...flag, mouseActive: false }), 100);
    setTimeout(() => {
      setFlag({
        ...flag,
        mouseActive: false,
        mouseActiveFade: false,
        mouseHover: false
      });
    }, 700);
  };

  return (
    <ListItem style={ListItemProps}>
      <LabelIcon name="copy" size="lg" />
      <CopiedText>Coppied!</CopiedText>
      <CopyOverlay
        style={LabelProps}
        onMouseOver={handleMouseOver}
        onMouseOut={handleMouseOut}
        onMouseDown={handleMouseDown}
        onMouseUp={handleMouseUp}
        onFocus={handleFocus}
        onBlur={handleBlur}
      >
        <LabelIcon name={iconType} size="lg" />
        <LabelTitle>{label}</LabelTitle>
        <LabelInput type="text" value={value} readOnly={readOnly} />
      </CopyOverlay>
    </ListItem>
  );
}

SideMenuListItem.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  value: PropTypes.any,
  iconType: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  readOnly: PropTypes.bool
};

SideMenuListItem.defaultProps = {
  value: undefined,
  readOnly: true
};

export default SideMenuListItem;
