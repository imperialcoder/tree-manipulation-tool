import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Layer } from 'react-konva';

const LayerComp = ({ stageRef, itemsToRender }) => {
  const stage = stageRef.current;

  useEffect(() => {
    stage.children.forEach(layer => layer.batchDraw());
  });

  return (
    <React.Fragment>
      <Layer name="layer-dynamic">{itemsToRender.dynamic.map(i => i)}</Layer>
      <Layer name="layer-static">{itemsToRender.static.map(i => i)}</Layer>
      <Layer name="layer-dragging" />
    </React.Fragment>
  );
};

LayerComp.propTypes = {
  stageRef: PropTypes.objectOf(PropTypes.any).isRequired,
  itemsToRender: PropTypes.objectOf(
    PropTypes.arrayOf(PropTypes.any),
    PropTypes.arrayOf(PropTypes.any)
  )
};

LayerComp.defaultProps = {
  itemsToRender: { static: [], dynamic: [] }
};

export default LayerComp;
