import React from 'react';
import { Switch, Route } from 'react-router-dom';
// import LoginPage from './components/pages/LoginPage';
import TreePage from './pages/TreePage';

// todo: switch route for production (instead of it directly going to treePage)

function App() {
  return (
    <div className="app-container">
      <Switch>
        <Route exact path="/" component={TreePage} />
        {/* <Route exact path="/tree" component={LoginPage} /> */}
      </Switch>
    </div>
  );
}

export default App;
