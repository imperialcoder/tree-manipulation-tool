/* eslint-disable no-unused-vars */
import React from 'react';
import PropTypes from 'prop-types';
import Konva from 'konva';
import { connect } from 'react-redux';
import { cloneDeep, sortBy } from 'lodash';
import { updateFormatedTreeData } from '../../redux/actions/treeActions';
import {
  organizeData,
  flattenOrganizedData,
  getChildrenElements
} from '../services/tree';
import RenderTreeComp from '../molecules/RenderTreeComp';

function TreeComp({ stageRef, tree, updateTreeData }) {
  // * state and globals

  const stage = stageRef.current;
  let hitNodes = [];

  // * Core Functions

  const getLayer = layerName => {
    return stage.children.filter(layer =>
      layer.attrs.name.includes(layerName) ? layer : undefined
    )[0];
  };

  const getAccountData = accountNumber => {
    const dataSource = tree.formatedData.data;
    return dataSource.filter(node => node.accountNumber === accountNumber)[0];
  };

  const getAccountNumber = nodeName => {
    const dashIndex = nodeName.indexOf('-') + 1;
    return Number(nodeName.slice(dashIndex, nodeName.length));
  };

  const getNodeSelection = node => {
    const result = [];
    const logic = n => {
      const { children } = n;
      result.push(n);
      if (children) {
        children.forEach(cn => logic(cn));
      }
    };
    logic(cloneDeep(node));
    return sortBy(result, n => n.depthLevel);
  };

  // * canvas filtering functions

  const filterCanvas = (canvasLayer, target, callback) =>
    canvasLayer.filter(node => callback(node, target)).map(n => n);

  const filterTargetable = target => {
    // filters nodes that can be targeted for a move
    const layerStatic = getLayer('static').children;
    return filterCanvas(layerStatic, target, (n, t) => {
      const includesTerm = n.attrs.name.includes('node');
      const nAccNumber = getAccountNumber(n.attrs.name);
      const tAccNumber = getAccountNumber(t.attrs.name);
      const tAccData = getAccountData(tAccNumber);
      const result = [];
      result.push(tAccData.sponsorAccountNumber);
      const logic = cn => {
        result.push(cn.accountNumber);
        const { children } = cn;
        if (children) {
          children.forEach(ccn => logic(ccn));
        }
      };
      logic(getAccountData(tAccNumber));
      return (
        includesTerm &&
        !result.includes(nAccNumber) &&
        nAccNumber !== tAccNumber
      );
    });
  };

  const filterTargetChildren = target => {
    // filters/grabs the children elements of a target
    const layerStatic = getLayer('static').children;
    return filterCanvas(layerStatic, target, (n, t) => {
      const includesTerm = n.attrs.name.includes('node');
      const nAccNumber = getAccountNumber(n.attrs.name);
      const tAccNumber = getAccountNumber(t.attrs.name);
      const result = [];
      const logic = cn => {
        result.push(cn.accountNumber);
        const { children } = cn;
        if (children) {
          children.forEach(ccn => logic(ccn));
        }
      };
      logic(getAccountData(tAccNumber));
      return (
        includesTerm && result.includes(nAccNumber) && nAccNumber !== tAccNumber
      );
    });
  };

  const filterTargetChildLines = target => {
    const layerDynamic = getLayer('dynamic').children;
    return filterCanvas(layerDynamic, target, (n, t) => {
      const includesTerm = n.attrs.name.includes('line');
      const nAccNumber = getAccountNumber(n.attrs.name);
      const tAccNumber = getAccountNumber(t.attrs.name);
      const result = [];
      const logic = cn => {
        result.push(cn.accountNumber);
        const { children } = cn;
        if (children) {
          children.forEach(ccn => logic(ccn));
        }
      };
      logic(getAccountData(tAccNumber));
      return (
        includesTerm && result.includes(nAccNumber) && nAccNumber !== tAccNumber
      );
    });
  };

  const getSponsorLine = target => {
    const layerDynamic = getLayer('dynamic').children;
    return filterCanvas(layerDynamic, target, (n, t) => {
      const includesTerm = n.attrs.name.includes('line');
      const nAccNumber = getAccountNumber(n.attrs.name);
      const tAccNumber = getAccountNumber(t.attrs.name);
      return includesTerm && nAccNumber === tAccNumber;
    })[0];
  };

  // * base logic for tree

  const checkForHit = target => {
    const filteredLayer = filterTargetable(target);

    const nodeExists = (hitNodeArray, name) => {
      let result = { value: false, index: undefined };
      hitNodeArray.forEach((hitNode, index) => {
        result =
          hitNode.canvasNode.name.includes(name.canvas) &&
          hitNode.targetNode.name.includes(name.target)
            ? { value: true, index }
            : { value: false, index };
      });
      return result;
    };

    const calcDistance = intersectObj =>
      Math.sqrt(
        // eslint-disable-next-line no-restricted-properties
        Math.pow(
          intersectObj.canvasNode.position.x -
            intersectObj.targetNode.position.x,
          2
        ) +
          // eslint-disable-next-line no-restricted-properties
          Math.pow(
            intersectObj.canvasNode.position.y -
              intersectObj.targetNode.position.y,
            2
          )
      );

    hitNodes = [];

    filteredLayer.forEach(node => {
      const nodeOrigin = node.position();
      const nodeChildOffset = node.findOne('.node-elements').position();
      const targetOrigin = target.position();
      const targetChildOffset = target.findOne('.node-elements').position();

      const intersectObj = {
        // todo: adjust the target and canvas nodes to include the offset inside the node for this calculation
        targetNode: {
          name: target.attrs.name,
          accountNumber: getAccountNumber(target.attrs.name),
          radius: target.findOne('.outer-circle').attrs.radius,
          position: {
            x: targetOrigin.x + targetChildOffset.x,
            y: targetOrigin.y + targetChildOffset.y
          }
        },
        canvasNode: {
          name: node.attrs.name,
          accountNumber: getAccountNumber(node.attrs.name),
          radius: node.findOne('.hit-box').attrs.radius,
          position: {
            x: nodeOrigin.x + nodeChildOffset.x,
            y: nodeOrigin.y + nodeChildOffset.y
          }
        }
      };
      const nameObj = {
        canvas: intersectObj.canvasNode.name,
        target: intersectObj.targetNode.name
      };
      const distance = calcDistance(intersectObj);
      const exists = nodeExists(hitNodes, nameObj);
      if (
        distance <
        intersectObj.canvasNode.radius + intersectObj.targetNode.radius
      ) {
        // console.log('its a hit!', exists); // if you need to see the output of the array
        if (!exists.value) {
          // if the value isn't in the hitNodes array, add it
          hitNodes.push(intersectObj);
        }
      }
    });
  };

  const updateSponsorLine = (target, data) => {
    // todo: adjust the target position to include it's inner offset (not just it's outer one)
    // this function controls how the sponsor line functions when dragging
    const sponsorLine = getSponsorLine(target);
    const sponsorData = getAccountData(data.sponsorAccountNumber);
    // check to see if the values exist
    if (sponsorData && sponsorLine) {
      const targetChildOffset = target.findOne('.node-elements').position();
      const targetPosition = target.position();
      const startPoint = {
        x: targetPosition.x + targetChildOffset.x,
        y: targetPosition.y + targetChildOffset.y
      };
      // if hitNodes, move endpoint to hitNode; otherwise to sponsor
      if (hitNodes.length > 0) {
        const endPoint = hitNodes[0].canvasNode.position;
        const lineOffset = {
          x: endPoint.x - startPoint.x,
          y: endPoint.y - startPoint.y
        };
        sponsorLine.strokeWidth(5);
        sponsorLine.points([0, 0, lineOffset.x, lineOffset.y]);
      } else {
        const endPoint = sponsorData.canvasPosition.current;
        const lineOffSet = {
          x: endPoint.x - startPoint.x,
          y: endPoint.y - startPoint.y
        };
        sponsorLine.strokeWidth(2);
        sponsorLine.points([0, 0, lineOffSet.x, lineOffSet.y]);
      }
      sponsorLine.position(startPoint);
      stage.findOne('.layer-dynamic').batchDraw();
    }
  };

  const calcLineOffset = (totalOffset, sponsor) => {
    let result;
    if (sponsor) {
      const sponsorOffset = {
        x: sponsor.canvasPosition.current.x - totalOffset.x,
        y: sponsor.canvasPosition.current.y - totalOffset.y
      };
      result = [0, 0, sponsorOffset.x, sponsorOffset.y];
    }
    return result;
  };

  const moveNode = targetData => {
    // 0) basic things that we just need
    const { targetNode, canvasNode } = hitNodes[0];
    const canvasData = getAccountData(canvasNode.accountNumber);
    // 1) get the selected Items in question
    const depthLevelDif = canvasData.depthLevel + 1 - targetData.depthLevel;
    const selectedItemData = [canvasData].concat(
      getNodeSelection(targetData).map((n, i) => {
        if (i === 0) {
          Object.assign(n, {
            sponsorAccountNumber: canvasData.accountNumber
          });
        }
        return Object.assign(n, {
          depthLevel: n.depthLevel + depthLevelDif
        });
      })
    );
    // 2) reassign selected items into a new datasource
    // note: Ik this peice of code is confusing XD it works tho
    const dataSource = sortBy(
      cloneDeep(tree.formatedData.data).map(selNode => {
        return (
          selectedItemData.filter(
            dataNode => selNode.accountNumber === dataNode.accountNumber
          )[0] || selNode
        );
      }),
      n => n.depthLevel
    );
    // 4) then update the formated data (which should trigger a rerender)
    updateTreeData(dataSource);
  };

  // * event handlers for the nodes (aka the glue to everything)

  const handleDragStart = (e, data, target) => {
    filterTargetable(target).forEach(node => {
      const hitBox = node.findOne('.hit-box');
      hitBox.to({
        opacity: 0.5
      });
    });
    // todo: figure out a way to animate the nodes going to sponsor node instead of Point of Orign
    filterTargetChildren(target).forEach(node => {
      node.to({
        duration: 0.2,
        easing: Konva.Easings.EaseOut,
        x: target.attrs.x,
        y: target.attrs.y,
        opacity: 0
      });
    });
    filterTargetChildLines(target).forEach(node => {
      node.to({
        duration: 0.2,
        easing: Konva.Easings.EaseOut,
        x: target.attrs.x,
        y: target.attrs.y,
        points: [0, 0, 0, 0],
        opacity: 0
      });
    });
  };

  const handleDragMove = (e, data, target) => {
    checkForHit(target);
    updateSponsorLine(target, data);
    const targetChildOffset = target.findOne('.node-elements').position();
    const targetPosition = target.position();
    const totalPositionOffset = {
      x: targetPosition.x + targetChildOffset.x,
      y: targetPosition.y + targetChildOffset.y
    };
    filterTargetChildren(target).forEach(node =>
      node.position(totalPositionOffset)
    );
    filterTargetChildLines(target).forEach(node =>
      node.position(totalPositionOffset)
    );
  };

  const handleDragEnd = (e, data, target) => {
    filterTargetable(target).forEach(node => {
      const hitBox = node.findOne('.hit-box');
      hitBox.to({
        opacity: 0
      });
    });
    if (hitNodes.length > 0) {
      moveNode(data);
    } else {
      const duration = 1.15;
      target.to({
        duration,
        easing: Konva.Easings.ElasticEaseOut,
        x: data.canvasPosition.current.x,
        y: data.canvasPosition.current.y
      });
      filterTargetChildren(target).forEach(canvasItem => {
        const accountNumber = getAccountNumber(canvasItem.attrs.name);
        const canvasItemData = getAccountData(accountNumber);
        canvasItem.to({
          duration,
          easing: Konva.Easings.ElasticEaseOut,
          x: canvasItemData.canvasPosition.current.x,
          y: canvasItemData.canvasPosition.current.y,
          opacity: 1
        });
      });
      filterTargetChildLines(target).forEach(canvasItem => {
        const accountData = getAccountData(
          getAccountNumber(canvasItem.attrs.name)
        );
        const start = accountData.canvasPosition.current;
        const end = getAccountData(accountData.sponsorAccountNumber)
          .canvasPosition.current;
        const lineOffSet = {
          x: end.x - start.x,
          y: end.y - start.y
        };
        canvasItem.to({
          duration,
          easing: Konva.Easings.ElasticEaseOut,
          x: start.x,
          y: start.y,
          points: [0, 0, lineOffSet.x, lineOffSet.y],
          opacity: 1
        });
      });
      const sponsor = getAccountData(data.sponsorAccountNumber);
      if (sponsor) {
        const lineOffset = calcLineOffset(data.canvasPosition.current, sponsor);
        const sponsorLine = getSponsorLine(target);
        sponsorLine.to({
          duration,
          easing: Konva.Easings.ElasticEaseOut,
          points: lineOffset,
          x: data.canvasPosition.current.x,
          y: data.canvasPosition.current.y
        });
      }
    }
  };

  return (
    <RenderTreeComp
      stageRef={stageRef}
      handle={{
        handleDragStart,
        handleDragMove,
        handleDragEnd
      }}
    />
  );
}

TreeComp.propTypes = {
  stageRef: PropTypes.objectOf(PropTypes.any).isRequired,
  tree: PropTypes.objectOf(PropTypes.any).isRequired,
  updateTreeData: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  tree: state.treeReducer
});

const mapDispatchToProps = dispatch => ({
  updateTreeData: formatedData => {
    dispatch(updateFormatedTreeData(formatedData));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TreeComp);
