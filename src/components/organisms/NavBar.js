/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSpring, animated } from 'react-spring';
import { connect } from 'react-redux';
import {
  initializeTreeData,
  initializeDummyData
} from '../../redux/actions/treeActions';
import SearchInput from '../molecules/SearchInput';
import TimeMachine from '../molecules/TimeMachine';
import DatePicker from '../molecules/DatePicker';

const Container = styled(animated.div)`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  width: 100%;
  box-shadow: 0px 0px 7px #595959;
`;

function NavBar({ initialState, tree, initTreeData, initDummyData }) {
  const [flag, setFlag] = useState({
    compact: false
  });

  const searchInputRef = React.createRef();

  const ContainerProps = useSpring({
    opacity: 1,
    height: flag.compact ? '6vh' : '100vh',
    backgroundColor: flag.compact ? '#4da6ff' : '#f2f2f2',
    from: { opacity: 0, height: '100vh', backgroundColor: '#f2f2f2' }
  });

  const handleSearch = () => {
    const searchInput = searchInputRef.current;
    if (searchInput.value.length > 0) {
      setFlag({ ...flag, compact: true });
      // todo: replace initDummyData with the one bellow
      initDummyData();
      // initTreeData(searchInput.value);
      initialState.set(true);
      searchInput.value = '';
    }
  };

  const handleEnter = e => handleSearch(e);

  const handleClick = e => handleSearch(e);

  return (
    <Container style={ContainerProps}>
      <SearchInput
        ref={searchInputRef}
        placeholder="Enter Account Number"
        handleEnter={handleEnter}
        handleClick={handleClick}
        value="30877"
      />
      <TimeMachine visible={!!flag.compact} />
      <DatePicker />
    </Container>
  );
}

NavBar.propTypes = {
  initialState: PropTypes.objectOf(PropTypes.any).isRequired,
  tree: PropTypes.objectOf(PropTypes.any).isRequired,
  initTreeData: PropTypes.func.isRequired,
  initDummyData: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  tree: state.treeReducer
});

const mapDispatchToProps = dispatch => ({
  initTreeData: accountNumber => {
    dispatch(initializeTreeData(accountNumber));
  },
  initDummyData: () => {
    dispatch(initializeDummyData());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavBar);
