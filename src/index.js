/* eslint-disable no-unused-vars */
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './redux/store';
import App from './components/App';
import style from './styles/index.css';

// use this for the current build of the tree
render(
  <BrowserRouter>
    <Provider store={store}>
      <App style={style} />
    </Provider>
  </BrowserRouter>,
  document.getElementById('root')
);

// use this for legacy build of the tree
// eslint-disable-next-line import/no-named-default
// import { default as Legacy } from './legacy/components/App';
// import { useRootContext } from './legacy/context/api/rootContext';

// render(
//   <useRootContext.Provider>
//     <Legacy />
//   </useRootContext.Provider>,
//   document.getElementById('root')
// );
